/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 13:40:43 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/05 15:28:29 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t	index;
	uint8_t	*u_dst;
	uint8_t	*u_src;
	uint8_t	u_c;

	index = 0;
	u_dst = (uint8_t *)dst;
	u_src = (uint8_t *)src;
	u_c = (uint8_t)c;
	while (index < n)
	{
		u_dst[index] = u_src[index];
		if (u_src[index] == u_c)
			return (&u_dst[index + 1]);
		index++;
	}
	return (NULL);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_buffer.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/05 17:05:44 by mvladymy          #+#    #+#             */
/*   Updated: 2018/02/21 14:11:02 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <ft_printf.h>
#include <libft.h>

int		manage_fd(int set_fd, bool set)
{
	static int	fd;

	if (set)
		fd = set_fd;
	return (fd);
}

void	manage_buffer(char c, bool gut)
{
	static char	buff[PRINTF_BUFF_SIZE];
	static int	pos;

	if (!gut)
		buff[pos++] = c;
	if (c == '\n' || gut == true || pos == PRINTF_BUFF_SIZE)
	{
		ADD_TO_COUNTER(pos);
		write(GET_FD, &buff, pos);
		pos = 0;
	}
}

int		manage_print_count(int cur_count, bool rewrite)
{
	static int	pritnted_char_count;

	if (rewrite)
	{
		cur_count = pritnted_char_count;
		pritnted_char_count = 0;
		return (cur_count);
	}
	else
	{
		pritnted_char_count += cur_count;
		return (0);
	}
}

void	push_line_to_buffer(char *s)
{
	while (*s != '\0')
		PUSH_TO_BUFFER(*s++);
}

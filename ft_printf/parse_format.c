/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_format.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/05 17:03:11 by mvladymy          #+#    #+#             */
/*   Updated: 2018/02/16 13:29:02 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>

void		parse_format(va_list *ap, char *format)
{
	while (*format != '\0')
	{
		while (*format != '%' && *format != '\0')
			PUSH_TO_BUFFER(*format++);
		if (*format == '%')
			format += parse_conv(format + 1, ap) + 1;
	}
	PUT_FROM_BUFFER(*format);
}

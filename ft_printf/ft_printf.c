/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/05 13:44:26 by mvladymy          #+#    #+#             */
/*   Updated: 2018/02/06 20:10:04 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>

int	ft_printf(const char *format, ...)
{
	va_list			ap;

	va_start(ap, format);
	SET_FD(1);
	parse_format(&ap, (char *)format);
	va_end(ap);
	return (GET_PRINT_COUNT);
}

int	ft_dprintf(int fd, const char *format, ...)
{
	va_list			ap;

	va_start(ap, format);
	SET_FD(fd);
	parse_format(&ap, (char *)format);
	va_end(ap);
	return (GET_PRINT_COUNT);
}

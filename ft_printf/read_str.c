/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_str.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/07 13:47:18 by mvladymy          #+#    #+#             */
/*   Updated: 2018/02/16 16:27:51 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>

void	read_str(char *str, t_conv *atr)
{
	size_t	str_len;
	size_t	printed_len;

	if (!str)
		str = "(null)";
	if (atr->conv_flags & CONV_FLAG_PRECS)
	{
		str_len = MIN(ft_strlen(str), (unsigned int)atr->conv_precision);
		printed_len = atr->conv_precision;
	}
	else
	{
		str_len = ft_strlen(str);
		printed_len = str_len;
	}
	if (!(atr->conv_flags & CONV_FLAG_NEG) && atr->conv_flags & CONV_FLAG_ZERO)
		ZERO_PADDING(atr->conv_width, str_len);
	else if (!(atr->conv_flags & CONV_FLAG_NEG))
		SPACE_PADDING(atr->conv_width, str_len);
	while (*str != '\0' && printed_len--)
		PUSH_TO_BUFFER(*str++);
	if (atr->conv_flags & CONV_FLAG_NEG)
		SPACE_PADDING(atr->conv_width, str_len);
}

void	read_char(unsigned char ch, t_conv *atr)
{
	if (!(atr->conv_flags & CONV_FLAG_NEG))
	{
		if (atr->conv_flags & CONV_FLAG_ZERO)
			ZERO_PADDING(atr->conv_width, 1);
		else
			SPACE_PADDING(atr->conv_width, 1);
	}
	PUSH_TO_BUFFER(ch);
	if (atr->conv_flags & CONV_FLAG_NEG)
		SPACE_PADDING(atr->conv_width, 1);
}

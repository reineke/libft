/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 14:40:43 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/07 17:35:55 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t destsize)
{
	size_t	index;
	size_t	d_len;
	size_t	s_len;

	index = 0;
	d_len = ft_strlen(dst);
	s_len = ft_strlen(src);
	if (d_len > destsize)
		return (s_len + destsize);
	while (src[index] != '\0' && destsize > (d_len + 1))
	{
		dst[d_len + index] = src[index];
		index++;
		destsize--;
	}
	dst[d_len + index] = '\0';
	return (d_len + s_len);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 09:36:36 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/07 15:37:49 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(const char *s, char (*f)(char))
{
	size_t	len;
	char	*map;

	if (!s || !f)
		return (NULL);
	len = ft_strlen(s);
	map = ft_strnew(len);
	if (!map)
		return (NULL);
	while (len--)
		map[len] = f(s[len]);
	return (map);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 14:24:42 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/06 12:48:34 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *s1, const char *s2, size_t n)
{
	size_t	idx;
	size_t	len;

	idx = 0;
	len = ft_strlen(s1);
	while (s2[idx] != '\0' && idx < n)
	{
		s1[len + idx] = s2[idx];
		idx++;
	}
	s1[len + idx] = '\0';
	return (s1);
}

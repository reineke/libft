/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/05 12:20:33 by mvladymy          #+#    #+#             */
/*   Updated: 2018/05/12 14:15:11 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <string.h>
# include <stdint.h>
# include <stdbool.h>
# include <unistd.h>
# include <wchar.h>
# include <stdlib.h>
# include <libft.h>

# define PRINTF_BUFF_SIZE 1024

typedef struct	s_conv {
	uint8_t		conv_type;
	uint16_t	conv_flags;
	int			conv_width;
	int			conv_precision;
}				t_conv;

# define CONV_FLAG_EMPTY 0b0000000000000000
# define CONV_FLAG_HASH  0b0000000000000001
# define CONV_FLAG_ZERO  0b0000000000000010
# define CONV_FLAG_NEG   0b0000000000000100
# define CONV_FLAG_POS   0b0000000000001000
# define CONV_FLAG_SPACE 0b0000000000010000
# define CONV_FLAG_LONG  0b0000000000100000
# define CONV_FLAG_LLONG 0b0000000001000000
# define CONV_FLAG_CHAR  0b0000000010000000
# define CONV_FLAG_SHORT 0b0000000100000000
# define CONV_FLAG_IMAX  0b0000001000000000
# define CONV_FLAG_SIZET 0b0000010000000000
# define CONV_FLAG_PRECS 0b0000100000000000

void			parse_format(va_list *ap, char *format);
size_t			parse_conv(char *conv, va_list *ap);
size_t			parse_atr(va_list *ap, char *conv_str, t_conv *conv_atr);

# define RA_WIDTH 0
# define RA_PRECS 1

void			manage_buffer(char c, bool gut);
void			push_line_to_buffer(char *s);
int				manage_print_count(int cur_count, bool reset);
int				manage_fd(int set_fd, bool set);

# define ITERATE_UNTIL(cond, iterator) while (cond) (iterator)++;

# define PUSH_TO_BUFFER(x) (manage_buffer(x, false))
# define PUT_FROM_BUFFER(x) (manage_buffer(x, true))
# define ADD_TO_COUNTER(x) (manage_print_count(x , false))
# define GET_PRINT_COUNT (manage_print_count(0 , true))
# define SET_FD(x) (manage_fd(x, true))
# define GET_FD	(manage_fd(0, false))

void			read_str(char *str, t_conv *atr);
void			read_char(unsigned char ch, t_conv *atr);
void			read_unistr(wchar_t *str, t_conv *atr);
void			read_unichar(wchar_t ch, t_conv *atr);
void			read_num(size_t num, t_conv *atr, uint8_t base, bool neg);
void			read_signed_num(long long int num, t_conv *atr, uint8_t base);
void			make_padding(unsigned int conv_width, unsigned int len,
						char filler);

# define ZERO_PADDING(width, len) (make_padding(MAX(0, width), len, '0'))
# define SPACE_PADDING(width, len) (make_padding(MAX(0, width), len, ' '))

# define READ_UNSIGNED_NUM(num, atr, base) (read_num(num, atr, base, false))

#endif

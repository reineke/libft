/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/01 15:29:30 by mvladymy          #+#    #+#             */
/*   Updated: 2018/03/19 12:51:09 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static unsigned int	count_parts(char *s, char c)
{
	unsigned int	qlty;

	qlty = 0;
	while (*s != '\0')
	{
		if (*s != c && (*(s + 1) == c || *(s + 1) == '\0'))
			qlty++;
		s++;
	}
	return (qlty);
}

static char			*cut_part(char **s, char c)
{
	size_t	len;
	char	*part;

	len = 0;
	while (**s == c && **s != '\0')
		(*s)++;
	while ((*s)[len] != c && (*s)[len] != '\0')
		len++;
	part = ft_strnew(len);
	if (!part)
		return (NULL);
	if (len)
	{
		ft_strncpy(part, *s, len);
		*s += len + 1;
	}
	return (part);
}

static void			split_abort(char ***s)
{
	char	**arg_ptr;

	if (!*s)
		return ;
	arg_ptr = *s;
	while (*arg_ptr)
		ft_strdel(arg_ptr++);
	ft_memdel((void **)s);
}

char				**ft_strsplit(const char *c_s, char c)
{
	char			**parted;
	char			*s;
	unsigned int	qlty;
	unsigned int	part;

	if (!c_s)
		return (NULL);
	s = (char *)c_s;
	qlty = count_parts(s, c);
	if (!(parted = (char **)ft_memalloc(sizeof(char *) * (qlty + 1))))
		return (NULL);
	part = 0;
	while (part < qlty)
	{
		parted[part] = cut_part(&s, c);
		if (!parted[part])
		{
			split_abort(&parted);
			return (NULL);
		}
		part++;
	}
	parted[part] = NULL;
	return (parted);
}

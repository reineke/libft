/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstradd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 12:54:07 by mvladymy          #+#    #+#             */
/*   Updated: 2018/02/16 12:07:27 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	**ft_lstradd(t_list **alst, t_list *new)
{
	t_list *lst;

	if (!alst || !new)
		return (NULL);
	if (!*alst)
	{
		*alst = new;
		return (alst);
	}
	lst = ft_lsttoend(*alst);
	lst->next = new;
	return (&lst->next);
}

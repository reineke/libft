/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memrealloc.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/10 13:14:46 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/10 13:21:13 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void	*ft_memrealloc(void *old, size_t size)
{
	void	*new;

	if (!(new = ft_memalloc(size)))
		return (NULL);
	ft_memmove(new, old, size);
	free(old);
	return (new);
}

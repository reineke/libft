/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/28 17:29:53 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/09 11:50:27 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include "libft.h"

int	ft_atoi(const char *str)
{
	int64_t		num;
	int			sign;
	size_t		spc;

	spc = 0;
	while (ft_isspace(str[spc]))
		spc++;
	sign = (str[spc] == '-') ? -1 : 1;
	if (str[spc] == '-' || str[spc] == '+')
		spc++;
	num = 0;
	while (ft_isdigit(str[spc]))
	{
		if ((num > (INT64_MAX / 10) ||
		(num >= (INT64_MAX / 10) && str[spc] >= '7')) && sign > 0)
			return (-1);
		if ((num > (INT64_MAX / 10) ||
		(num >= (INT64_MAX / 10) && str[spc] >= '8')) && sign < 0)
			return (0);
		num = (num * 10) + (str[spc++] - '0');
	}
	return ((int)num * sign);
}

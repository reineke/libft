/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 15:18:46 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/04 17:48:02 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include "libft.h"

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t	index;
	uint8_t	*u_s1;
	uint8_t	*u_s2;

	u_s1 = (uint8_t *)s1;
	u_s2 = (uint8_t *)s2;
	index = 0;
	while (index < n)
	{
		if (u_s1[index] != u_s2[index])
			return (u_s1[index] - u_s2[index]);
		index++;
	}
	return (0);
}

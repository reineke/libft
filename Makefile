# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/10/26 16:15:33 by mvladymy          #+#    #+#              #
#    Updated: 2019/07/03 12:29:43 by mvladymy         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re $(NAME)

NAME=libft.a
CFLAGS=-Iinclude -Wall -Wextra -Werror -Wconversion -Os
MAIN_SRC=ft_bzero.c ft_isdigit.c ft_memdel.c ft_isspace.c ft_iswhitespace.c \
	ft_putstr.c ft_strcpy.c ft_striter.c ft_strlen.c ft_strncmp.c ft_strsub.c \
	ft_isalnum.c ft_isprint.c ft_memset.c ft_strcat.c ft_strdel.c \
	ft_striteri.c ft_strmap.c ft_strncpy.c ft_tolower.c ft_isalpha.c \
	ft_memalloc.c ft_putchar.c ft_strclr.c ft_strdup.c ft_strjoin.c \
	ft_strmapi.c ft_strnequ.c ft_toupper.c ft_isascii.c ft_memcpy.c \
	ft_putendl.c ft_strcmp.c ft_strequ.c ft_strlcat.c ft_strncat.c ft_strnew.c \
	ft_atoi.c ft_strstr.c ft_strnstr.c ft_strchr.c ft_strrchr.c \
	ft_putchar_fd.c ft_putstr_fd.c ft_putendl_fd.c ft_memccpy.c ft_memchr.c \
	ft_memcmp.c ft_strtrim.c ft_itoa.c ft_putnbr.c ft_putnbr_fd.c ft_memmove.c \
	ft_strsplit.c ft_lstadd.c ft_lstdel.c ft_lstdelone.c ft_lstiter.c \
	ft_lstmap.c ft_lstnew.c ft_lsttoend.c ft_lstradd.c ft_memrealloc.c \
	ft_strsubst.c ft_count_digits.c get_next_line.c ft_lstdelfirst.c \
	ft_itoa_buff.c ft_stradd.c ft_putwchar.c ft_putwchar_fd.c ft_wcstrlen.c \
	ft_getwchar.c ft_getwchar_fd.c ft_perror.c
FT_PRINTF_SRC=ft_printf.c parse_conv.c read_str.c make_padding.c \
	parse_format.c read_unicode.c manage_buffer.c read_num.c parse_atr.c \
	read_signed_num.c
FT_PRINTF_SRC:=$(FT_PRINTF_SRC:%=ft_printf/%)
MAIN_OBJ=$(MAIN_SRC:.c=.o)
FT_PRINTF_OBJ=$(FT_PRINTF_SRC:.c=.o)
MAIN_HEADERS=include/libft.h
FT_PRINTF_HEADERS=include/ft_printf.h

all: $(NAME)

$(NAME): $(MAIN_OBJ) $(FT_PRINTF_OBJ)
	@ar rc $@ $(MAIN_OBJ) $(FT_PRINTF_OBJ)

ft_printf/%.o: ft_printf/%.c $(MAIN_HEADERS) $(FT_PRINTF_HEADERS)
	@$(CC) $(filter-out -Wconversion, $(CFLAGS)) -c $< -o $@

./%.o: ./%.c $(MAIN_HEADERS)
	@$(CC) $(CFLAGS) -c $< -o $@

clean:
	@rm -f $(MAIN_OBJ) $(FT_PRINTF_OBJ)

fclean: clean
	@rm -f $(NAME)

re: fclean
	@$(MAKE) all
